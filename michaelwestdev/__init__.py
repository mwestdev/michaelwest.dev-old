from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_bcrypt import Bcrypt
from flask_login import LoginManager
from flask_mail import Mail
from michaelwestdev.config import Config
from flask_assets import Environment
from flask_limiter import Limiter
from flask_limiter.util import get_remote_address
from flask_migrate import Migrate


bcrypt = Bcrypt()
db = SQLAlchemy()
migrate = Migrate()
login_manager = LoginManager()
login_manager.login_view = 'users.login'
login_manager.login_message_category = 'info'
mail = Mail()
assets = Environment()
limiter = Limiter(key_func=get_remote_address)


def create_app(config_class=Config):

    app = Flask(__name__)
    app.config.from_object(Config)

    bcrypt.init_app(app)
    db.init_app(app)
    migrate.init_app(app, db)
    login_manager.init_app(app)
    mail.init_app(app)
    assets.init_app(app)
    limiter.init_app(app)

    from michaelwestdev.utils import assetbundles

    from michaelwestdev.users.routes import users
    from michaelwestdev.posts.routes import posts
    from michaelwestdev.creations.routes import creations
    from michaelwestdev.scripts.routes import scripts
    from michaelwestdev.main.routes import main
    from michaelwestdev.errors.handlers import errors
    app.register_blueprint(users)
    app.register_blueprint(posts)
    app.register_blueprint(creations)
    app.register_blueprint(scripts)
    app.register_blueprint(main)
    app.register_blueprint(errors)

    return app
