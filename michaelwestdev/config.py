# Set the Environment Variables listed here on your local machine
# export ENVIRONMENT_VARABLE_NAME = "ENVIRONMENT_VARABLE_VALUE"
# in your .bashrc
import json
import os

with open("/etc/michaelwestdev-config.json") as config_file:
    config = json.load(config_file)


class Config:

    # Define the database
    PROJECT_ROOT = os.path.dirname(os.path.abspath(__file__))
    BASE_DIR = os.path.dirname(PROJECT_ROOT)

    DEBUG = config.get("DEBUG")

    if DEBUG:
        SQLALCHEMY_DATABASE_URI = "sqlite:///" + os.path.join(
            BASE_DIR, config.get("SQLALCHEMY_DATABASE_NAME")
        )
    else:
        SQLALCHEMY_DATABASE_URI = config.get("SQLALCHEMY_DATABASE_URI")

    DATABASE_CONNECT_OPTIONS = {}

    # Check overhead before enabling this in production
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    # Application threads. A common general assumption is
    # using 2 per available processor cores - to handle
    # incoming requests using one and performing background
    # operations using the other.
    THREADS_PER_PAGE = 2

    # Enable protection agains *Cross-site Request Forgery (CSRF)*
    CSRF_ENABLED = True

    # Use a secure, unique and absolutely secret key for
    # signing the data.
    CSRF_SESSION_KEY = config.get("CSRF_SESSION_KEY")

    # Secret key for signing cookies
    SECRET_KEY = config.get("SECRET_KEY")

    # Mail, for reset password & contact form
    MAIL_SERVER = config.get("MAIL_SERVER")
    MAIL_PORT = 587
    MAIL_USE_TLS = True
    MAIL_USERNAME = config.get("MAIL_USERNAME")
    MAIL_PASSWORD = config.get("MAIL_PASSWORD")
    MAIL_RECIPIENT_USERNAME = config.get("MAIL_RECIPIENT_USERNAME")
