from flask import Blueprint, render_template, request, send_from_directory
from michaelwestdev.models import Creation, Post, Script

main = Blueprint("main", __name__)


@main.route("/")
def home():
    latest_posts = Post.query.order_by(Post.date_posted.desc()).limit(4).all()
    latest_creations = (
        Creation.query.order_by(Creation.date_posted.desc()).limit(4).all()
    )
    latest_scripts = Script.query.order_by(Script.date_posted.desc()).limit(4).all()
    return render_template(
        "home.html",
        latest_posts=latest_posts,
        latest_creations=latest_creations,
        latest_scripts=latest_scripts,
    )


@main.route("/posts")
def posts():
    page = request.args.get("page", 1, type=int)
    posts = Post.query.order_by(Post.date_posted.desc()).paginate(page=page, per_page=4)
    return render_template("posts.html", posts=posts)


@main.route("/creations")
def creations():
    page = request.args.get("page", 1, type=int)
    creations = Creation.query.order_by(Creation.date_posted.desc()).paginate(
        page=page, per_page=4
    )
    return render_template("creations.html", creations=creations)


@main.route("/scripts")
def scripts():
    page = request.args.get("page", 1, type=int)
    scripts = Script.query.order_by(Script.date_posted.desc()).paginate(
        page=page, per_page=4
    )
    return render_template("scripts.html", scripts=scripts)


@main.route("/robots.txt")
def robots():
    return send_from_directory("static", "robots.txt")
