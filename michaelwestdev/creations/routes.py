from flask import Blueprint, abort, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from michaelwestdev import db
from michaelwestdev.creations.forms import CreationForm
from michaelwestdev.models import Creation

creations = Blueprint("creations", __name__)


@creations.route("/creation/new", methods=["GET", "POST"])
@login_required
def new_creation():
    form = CreationForm()
    if form.validate_on_submit():
        creation = Creation(
            title=form.title.data,
            url=form.url.data,
            description=form.description.data,
            author=current_user,
        )
        db.session.add(creation)
        db.session.commit()
        flash("Your creation has been posted!", "success")
        return redirect(url_for("main.creations"))
    return render_template(
        "create_creation.html", title="New Creation", form=form, legend="New Creation"
    )


@creations.route("/creation/<int:creation_id>")
def creation(creation_id):
    creation = Creation.query.get_or_404(creation_id)
    return render_template("creation.html", title=creation.title, creation=creation)


@creations.route("/creation/<int:creation_id>/update", methods=["GET", "POST"])
@login_required
def update_creation(creation_id):
    creation = Creation.query.get_or_404(creation_id)
    if creation.author != current_user:
        abort(403)
    form = CreationForm()
    if form.validate_on_submit():
        creation.title = form.title.data
        creation.url = form.url.data
        creation.description = form.description.data
        db.session.commit()
        flash("Your creation has been updated!", "success")
        return redirect(url_for("creations.creation", creation_id=creation.id))
    elif request.method == "GET":
        form.title.data = creation.title
        form.url.data = creation.url
        form.description.data = creation.description
    return render_template(
        "create_creation.html",
        title="Update Creation",
        form=form,
        legend="Update Creation",
    )


@creations.route("/creation/<int:creation_id>/delete", methods=["POST"])
@login_required
def delete_creation(creation_id):
    creation = Creation.query.get_or_404(creation_id)
    if creation.author != current_user:
        abort(403)
    db.session.delete(creation)
    db.session.commit()
    flash("Your creation has been deleted!", "success")
    return redirect(url_for("main.creations"))
