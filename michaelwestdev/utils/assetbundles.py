from flask_assets import Bundle
from michaelwestdev import assets

bundles = {
    "js_main": Bundle(
        "js/navbar.js",
        "js/ajaxPostsPagination.js",
        "js/ajaxCreationsPagination.js",
        "js/ajaxScriptsPagination.js",
        filters="jsmin",
        output="gen/main.min.js",
    ),
    "css_main": Bundle(
        "styles/main.scss",
        depends="styles/*.scss",
        filters="pyscss, cssmin",
        output="gen/main.min.css",
    ),
}
assets.register(bundles)
