from flask import Blueprint, abort, flash, redirect, render_template, request, url_for
from flask_login import current_user, login_required
from michaelwestdev import db
from michaelwestdev.models import Script
from michaelwestdev.scripts.forms import ScriptForm

scripts = Blueprint("scripts", __name__)


@scripts.route("/script/new", methods=["GET", "POST"])
@login_required
def new_script():
    form = ScriptForm()
    if form.validate_on_submit():
        script = Script(
            title=form.title.data,
            url=form.url.data,
            description=form.description.data,
            author=current_user,
        )
        db.session.add(script)
        db.session.commit()
        flash("Your script has been posted!", "success")
        return redirect(url_for("main.scripts"))
    return render_template(
        "create_script.html", title="New script", form=form, legend="New Script"
    )


@scripts.route("/script/<int:script_id>")
def script(script_id):
    script = Script.query.get_or_404(script_id)
    return render_template("script.html", title=script.title, script=script)


@scripts.route("/script/<int:script_id>/update", methods=["GET", "POST"])
@login_required
def update_script(script_id):
    script = Script.query.get_or_404(script_id)
    if script.author != current_user:
        abort(403)
    form = ScriptForm()
    if form.validate_on_submit():
        script.title = form.title.data
        script.url = form.url.data
        script.description = form.description.data
        db.session.commit()
        flash("Your script has been updated!", "success")
        return redirect(url_for("scripts.script", script_id=script.id))
    elif request.method == "GET":
        form.title.data = script.title
        form.url.data = script.url
        form.description.data = script.description
    return render_template(
        "create_script.html", title="Update script", form=form, legend="Update script"
    )


@scripts.route("/script/<int:script_id>/delete", methods=["POST"])
@login_required
def delete_script(script_id):
    script = Script.query.get_or_404(script_id)
    if script.author != current_user:
        abort(403)
    db.session.delete(script)
    db.session.commit()
    flash("Your script has been deleted!", "success")
    return redirect(url_for("main.scripts"))
