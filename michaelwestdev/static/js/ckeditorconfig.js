/* global CKEDITOR */
(function() {
    CKEDITOR.plugins.addExternal(
        "codesnippet",
        "/static/plugins/ckeditor/codesnippet/",
        "plugin.js"
    );
    CKEDITOR.plugins.addExternal(
        "youtube",
        "/static/plugins/ckeditor/youtube/",
        "plugin.js"
    );
    CKEDITOR.replace("editor", {
        extraPlugins: "codesnippet,youtube",
    });
    CKEDITOR.config.codeSnippet_languages = {
        python: "Python",
        javascript: "JavaScript",
        bash: "Bash",
        html: "HTML",
        css: "CSS",
    };
    CKEDITOR.config.codeSnippet_theme = "dark";
    CKEDITOR.config.youtube_responsive = true;
})();
