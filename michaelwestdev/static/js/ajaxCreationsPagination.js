function ajaxCreationsPagination() {
    $("#creations-pagination a").each((index, el) => {
        $(el).click(e => {
            e.preventDefault();
            let page_url = $(el).attr("href");
            console.log(page_url);

            $.ajax({
                url: page_url,
                type: "GET",
                success: data => {
                    $("#creations").fadeOut(500, function() {
                        $(this).empty();
                        $(this)
                            .append(
                                $(data)
                                    .find("#creations")
                                    .html()
                            )
                            .fadeIn(500);
                    });

                    $("#creations-pagination").empty();
                    $("#creations-pagination").append(
                        $(data)
                            .find("#creations-pagination")
                            .html()
                    );
                },
            });
        });
    });
}

$(document).ready(function() {
    ajaxCreationsPagination();
});
$(document).ajaxStop(function() {
    ajaxCreationsPagination();
});
