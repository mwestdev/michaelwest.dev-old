$(document).ready(function() {
    // Highlight current page on navbar

    $(".navbar-nav a")
        .filter(function() {
            var pathArray = window.location.pathname.split("/");

            if (this.href.indexOf(pathArray[1]) > 0) {
                return true;
            }
        })
        .addClass("active");
});
