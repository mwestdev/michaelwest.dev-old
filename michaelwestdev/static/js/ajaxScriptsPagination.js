function ajaxScriptsPagination() {
    $("#scripts-pagination a").each((index, el) => {
        $(el).click(e => {
            e.preventDefault();
            let page_url = $(el).attr("href");
            console.log(page_url);

            $.ajax({
                url: page_url,
                type: "GET",
                success: data => {
                    $("#scripts").fadeOut(500, function() {
                        $(this).empty();
                        $(this)
                            .append(
                                $(data)
                                    .find("#scripts")
                                    .html()
                            )
                            .fadeIn(500);
                    });

                    $("#scripts-pagination").empty();
                    $("#scripts-pagination").append(
                        $(data)
                            .find("#scripts-pagination")
                            .html()
                    );
                },
            });
        });
    });
}

$(document).ready(function() {
    ajaxScriptsPagination();
});
$(document).ajaxStop(function() {
    ajaxScriptsPagination();
});
