# Public repository for my website: [michaelwest.dev](https://michaelwest.dev) powered by Flask.

### Clone this repo:
```
git clone https://gitlab.com/mwestdev/michaelwest.dev.git ~/michaelwest.dev
```

### Set up python virtual environment:
```
cd ~/michaelwest.dev
python3 -m venv venv
source venv/bin/activate
pip install -r requirements.txt
```
### Create config file:
```
sudo touch /etc/michaelwestdev-config.json
sudo vi /etc/michaelwestdev-config.json
```

```json
{
	"SECRET_KEY":"SECRET_KEY_HERE",
	"CSRF_SESSION_KEY":"CSRF_SESSION_KEY_HERE",
	"SQLALCHEMY_DATABASE_NAME":"SQLALCHEMY_DATABASE_NAME_HERE",
	"MAIL_USERNAME":"MAIL_USERNAME_HERE",
	"MAIL_PASSWORD":"MAIL_PASSWORD_HERE",
	"MAIL_RECIPIENT_USERNAME":"MAIL_RECIPIENT_USERNAME_HERE"
}
```

### Run this app locally:
```
cd ~/michaelwest.dev
source venv/bin/activate
python3 run.py
```

### App should now be running at:

```
http://localhost:5000/
```
